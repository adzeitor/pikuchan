package main

import (
	"database/sql"

	_ "github.com/lib/pq"
)

type PostgresAccounts struct {
	DB *sql.DB
}

type PostgresImages struct {
	DB *sql.DB
}

// ConnectPostgres connects to postgres database
func ConnectPostgres(uri string) (*sql.DB, error) {
	db, err := sql.Open("postgres", uri)

	db.SetMaxOpenConns(10)

	return db, err
}

// Insert add user account to postgresql storage
func (s *PostgresAccounts) Insert(account Account) (Account, error) {
	err := s.DB.QueryRow(
		`INSERT INTO accounts (username, email, age, created_at, updated_at)
        VALUES ($1, $2, $3, $4, $5) RETURNING id
		`, account.Username, account.Email, account.Age, account.CreatedAt, account.UpdatedAt).Scan(&account.ID)

	return account, err
}

// Update update account information
func (s *PostgresAccounts) Update(account Account) error {
	_, err := s.DB.Exec(
		`UPDATE accounts SET username=$1, email=$2, age=$3, updated_at=$4
		`, account.Username, account.Email, account.Age, account.UpdatedAt)

	return err
}

// Get retrieve user account from postgresql storage
func (s *PostgresAccounts) Get(id string) (Account, error) {
	var account Account

	err := s.DB.QueryRow(
		`SELECT id, username, email, age, created_at, updated_at
         FROM accounts
         WHERE id = $1
         LIMIT 1`, id).Scan(&account.ID, &account.Username, &account.Email, &account.Age, &account.CreatedAt, &account.UpdatedAt)

	return account, err
}

func (s *PostgresAccounts) All() ([]Account, error) {
	rows, err := s.DB.Query(`
SELECT id, username, email, age, created_at, updated_at
FROM accounts
`)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	items := []Account{}

	for rows.Next() {
		var account Account

		rows.Scan(&account.ID, &account.Username, &account.Email, &account.Age, &account.CreatedAt, &account.UpdatedAt)
		items = append(items, account)
	}

	return items, nil
}

// Insert add user account to postgresql storage
func (s *PostgresImages) Insert(image Image) (Image, error) {
	err := s.DB.QueryRow(
		`INSERT INTO images (account_id, mime_type, created_at)
        VALUES ($1,$2,$3) RETURNING id
		`, image.AccountID, image.MimeType, image.CreatedAt).Scan(&image.ID)

	return image, err
}

// Get retrieve user account from postgresql storage
func (s *PostgresImages) Get(id string) (Image, error) {
	var image Image

	err := s.DB.QueryRow(
		`SELECT id, account_id, mime_type, created_at
         FROM images
         WHERE id = $1
         LIMIT 1`, id).Scan(&image.ID, &image.AccountID, &image.MimeType, &image.CreatedAt)

	return image, err
}

func (s *PostgresImages) Count() (int64, error) {
	count := int64(0)

	err := s.DB.QueryRow(
		`SELECT COUNT(id)
         FROM images`).Scan(&count)

	return count, err
}

func (s *PostgresImages) CountByAccountID(id string) (int64, error) {
	count := int64(0)

	err := s.DB.QueryRow(
		`SELECT COUNT(id) FROM images WHERE account_id = $1`, id).Scan(&count)

	return count, err
}

func (s *PostgresImages) ByCreatedAt(limit int) ([]Image, error) {
	rows, err := s.DB.Query(`
SELECT id, account_id, mime_type, created_at
FROM images
ORDER BY created_at DESC
LIMIT $1
`, limit)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	items := []Image{}

	for rows.Next() {
		var image Image

		rows.Scan(&image.ID, &image.AccountID, &image.MimeType, &image.CreatedAt)
		items = append(items, image)
	}

	return items, nil
}
