package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
)

type LocalFileStorage struct {
	Dir       string
	URIPrefix string
}

func (s LocalFileStorage) Prepare() error {
	err := os.MkdirAll(s.Dir, 0700)
	return err
}

func (s LocalFileStorage) Store(id string, content io.Reader) error {
	f, err := os.Create(filepath.Join(s.Dir, id))

	if err != nil {
		return err
	}

	_, err = io.Copy(f, content)
	f.Close()
	return err
}

func (s LocalFileStorage) Get(id string) (io.ReadSeeker, error) {
	f, err := os.Open(filepath.Join(s.Dir, id))
	if err != nil {
		return nil, err
	}

	content, err := ioutil.ReadAll(f)
	f.Close()
	return bytes.NewReader(content), err
}

func (s LocalFileStorage) URI(image Image) string {
	return path.Join(s.URIPrefix, image.ID)
}
