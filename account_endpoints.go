package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

const minAge = 18

type signupRequest struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Age      int    `json:"age"`
}

type signupResponse struct {
	ID string `json:"id"`
}

func signupEndpoint(app *App) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var sr signupRequest

		err := json.NewDecoder(r.Body).Decode(&sr)
		if err != nil {
			jsonError(w, "body should be json object", http.StatusBadRequest)
			return
		}

		if sr.Age < minAge {
			jsonError(w, fmt.Sprintf("age must be greater than %d", minAge), http.StatusBadRequest)
			return
		}

		now := time.Now()
		account, err := app.Accounts.Insert(Account{
			Username:  sr.Username,
			Email:     sr.Email,
			Age:       sr.Age,
			CreatedAt: now,
			UpdatedAt: now,
		})
		if err != nil {
			jsonError(w, "database error", http.StatusInternalServerError)
			log.Println(err)
			return
		}

		json.NewEncoder(w).Encode(signupResponse{
			ID: account.ID,
		})
	}
}

type getAccountResponse struct {
	ID       string  `json:"id"`
	Username string  `json:"username"`
	Rating   float64 `json:"rating"`
}

func getAccountEndpoint(app *App) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		account, err := app.Accounts.Get(r.FormValue("id"))
		if err != nil {
			jsonError(w, fmt.Sprintf("account %q not found", r.FormValue("id")), http.StatusBadRequest)
			return
		}

		json.NewEncoder(w).Encode(getAccountResponse{
			ID:       account.ID,
			Username: account.Username,
			Rating:   app.CalcRating(account),
		})
	}
}

type listAccountResponse struct {
	Username string  `json:"username"`
	Rating   float64 `json:"rating"`
}

func accountsListEndpoint(app *App) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		accounts, err := app.Accounts.All()
		if err != nil {
			jsonError(w, "database eror", http.StatusBadRequest)
			log.Println(err)
			return
		}

		items := make([]listAccountResponse, len(accounts))

		for i := range accounts {
			items[i] = listAccountResponse{
				Username: accounts[i].Username,
				Rating:   app.CalcRating(accounts[i]),
			}
		}

		json.NewEncoder(w).Encode(items)
	}
}
