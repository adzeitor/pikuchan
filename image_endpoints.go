package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image/jpeg"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type uploadResponse struct {
	URI    string  `json:"uri"`
	Rating float64 `json:"rating"`
}

func uploadImageEndpoint(app *App) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		account, err := app.Accounts.Get(r.FormValue("id"))
		if err != nil {
			jsonError(w, fmt.Sprintf("account %q not found", r.FormValue("id")), http.StatusBadRequest)
			return
		}

		err = r.ParseMultipartForm(app.MaxUploadSize)
		if err != nil {
			jsonError(w, err.Error(), http.StatusBadRequest)
			return
		}

		for _, formFiles := range r.MultipartForm.File {
			for _, file := range formFiles {
				content, err := file.Open()
				if err != nil {
					jsonError(w, err.Error(), http.StatusUnprocessableEntity)
					return
				}
				defer content.Close()

				bs, err := ioutil.ReadAll(content)
				if err != nil {
					jsonError(w, err.Error(), http.StatusUnprocessableEntity)
					return
				}

				_, err = jpeg.Decode(bytes.NewBuffer(bs))
				if err != nil {
					jsonError(w, "image should be in jpeg format", http.StatusUnsupportedMediaType)
					return
				}

				image, err := app.Images.Insert(Image{
					AccountID: account.ID,
					MimeType:  "image/jpeg",
					CreatedAt: time.Now(),
					UpdatedAt: time.Now(),
				})
				if err != nil {
					jsonError(w, "database error", http.StatusInternalServerError)
					log.Println(err)
					return
				}

				err = app.Files.Store(image.ID, bytes.NewBuffer(bs))
				if err != nil {
					jsonError(w, "image should be in jpeg format", http.StatusUnsupportedMediaType)
					return
				}

				json.NewEncoder(w).Encode(uploadResponse{
					URI:    app.Files.URI(image),
					Rating: app.CalcRating(account),
				})
				// only one image per request
				return
			}
		}

		jsonError(w, "no image. Use multipart form data", http.StatusBadRequest)
	}
}
