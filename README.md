

# Example

https://pikuchan.herokuapp.com

# installation
[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)


## Environment variables

- `PORT` http port
- `BIND` bind on address (by default all addresses `0.0.0.0`)
- `DATABASE_URL` postgresql URL in format `postgres://user:pass@host/database`
- `FILESTORAGE` (by default `media` in current directory)

    
### AWS s3 for file storage

Set `FILESTORAGE=S3` and:
    
+ `S3_ACCESSKEY`
+ `S3_SECRETKEY`
+ `S3_ENDPOINT`
+ `S3_REGION`
+ `S3_BUCKET`

## run in docker

first run:

```bash
$ go get gitlab.com/foldzip/pikuchan
$ cd $GOPATH/src/gitlab.com/foldzip/pikuchan
$ docker-compose up db
```

After postgresql initialized press `CTRL+C`, then:

```
$ docker-compose up
```

## Run tests in docker

```bash
$ docker-compose -f docker-compose.test.yml up
```

### Clean

```bash
$ docker-compose rm
```


## API

### Create account


```bash
$ curl localhost:3000/account -X PUT -d '{"username":"lora_palmer","age":18, "email": "lora@example.com"}'
```

```json
{"id":"1c231b29-a64a-4a41-8841-88ec6204db30"}
```

### Upload image

#### params

- `id`: account id 


#### example 

```bash
$ curl -F "image=@./picture.jpeg" "localhost:3000/upload?id=1c231b29-a64a-4a41-8841-88ec6204db30"
```

```json
{"uri":"/media/7d791594-2dd3-48b6-b076-500e4607d120", "rating":74.17}
```


### Get account info

#### params

- `id`: account id 


#### example 

```bash
curl pikuchan.herokuapp.com/account?id=1c231b29-a64a-4a41-8841-88ec6204db30
```

```json
{"id":"1c231b29-a64a-4a41-8841-88ec6204db30","username":"bob","rating":74.17}
```

### All accounts

```bash
$ curl -F "image=@./picture.jpeg" "localhost:3000/accounts"
```

```json
[
    {"username":"bob", "rating":74.17},
    {"username":"lora", "rating":25.83}
]
```

### Errors

On errors all json endpoints returns non-200 http status code and json:

```json
{
    message: "error message"
}
```
