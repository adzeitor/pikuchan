
#!/bin/bash

psql "$DATABASE_URL" << EOF
    CREATE EXTENSION "pgcrypto";


    CREATE TABLE IF NOT EXISTS accounts(
        id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
        username TEXT,
        email TEXT,
        age INT,
        rating INT,
        created_at timestamp with time zone,
        updated_at timestamp with time zone
    ) ;

    CREATE TABLE IF NOT EXISTS images(
        id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
        account_id text,
        mime_type TEXT,
        created_at timestamp with time zone,
        updated_at timestamp with time zone
    );
EOF
