package main

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

func setup() *App {
	pg, err := ConnectPostgres(os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal(err)
	}

	fileStorage := LocalFileStorage{
		Dir:       "/tmp/pikuchan_test/",
		URIPrefix: "/media",
	}

	err = fileStorage.Prepare()
	if err != nil {
		log.Fatal(err)
	}

	app := &App{
		Accounts:      &PostgresAccounts{pg},
		Images:        &PostgresImages{pg},
		MaxUploadSize: 10 * 1024 * 1024 * 1024,
		Files:         fileStorage,
	}

	return app
}

func TestEndpoints(t *testing.T) {
	app := setup()
	client := &http.Client{}

	var createdAccount Account
	t.Run("Sign Up", func(t *testing.T) {
		server := httptest.NewServer(http.HandlerFunc(signupEndpoint(app)))
		defer server.Close()
		request, err := http.NewRequest("PUT", server.URL, strings.NewReader(`{"username":"lora_palmer","age":18, "email": "lora@twinpeaks.com"}`))
		if err != nil {
			t.Fatal(err)
		}

		resp, err := client.Do(request)

		if resp.StatusCode != 200 {
			t.Fatalf("Received non-200 response: %d\n", resp.StatusCode)
		}
		err = json.NewDecoder(resp.Body).Decode(&createdAccount)
		if err != nil {
			log.Fatal(err)
		}
	})

	t.Run("Get account", func(t *testing.T) {
		server := httptest.NewServer(http.HandlerFunc(getAccountEndpoint(app)))
		defer server.Close()
		resp, err := http.Get(server.URL + "?id=" + createdAccount.ID)
		if err != nil {
			log.Fatal(err)
		}

		if resp.StatusCode != 200 {
			t.Fatalf("Received non-200 response: %d\n", resp.StatusCode)
		}
	})

	t.Run("Get list of accounts", func(t *testing.T) {
		server := httptest.NewServer(http.HandlerFunc(accountsListEndpoint(app)))
		defer server.Close()
		resp, err := http.Get(server.URL)
		if err != nil {
			log.Fatal(err)
		}

		if resp.StatusCode != 200 {
			t.Fatalf("Received non-200 response: %d\n", resp.StatusCode)
		}
	})
}
