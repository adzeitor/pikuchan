package main

import (
	"io"
	"net/url"

	"io/ioutil"

	"bytes"
	"path"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type S3FileStorage struct {
	AccessKey string
	SecretKey string
	Endpoint  string
	Region    string
	Bucket    string

	S3Config  *aws.Config
	S3Session *session.Session
}

func (s *S3FileStorage) Connect() error {
	s.S3Config = &aws.Config{
		Credentials:      credentials.NewStaticCredentials(s.AccessKey, s.SecretKey, ""),
		Endpoint:         aws.String(s.Endpoint),
		Region:           aws.String(s.Region),
		DisableSSL:       aws.Bool(true),
		S3ForcePathStyle: aws.Bool(true),
	}
	s.S3Session = session.New(s.S3Config)
	return nil
}

func (s S3FileStorage) Store(id string, content io.Reader) error {
	bs, err := ioutil.ReadAll(content)

	if err != nil {
		return err
	}

	s3Client := s3.New(s.S3Session)
	_, err = s3Client.PutObject(&s3.PutObjectInput{
		Body:   bytes.NewReader(bs),
		Bucket: &s.Bucket,
		Key:    &id,
		ACL:    aws.String("public-read"),
	})
	return err
}

func (s S3FileStorage) URI(image Image) string {
	u, _ := url.Parse(s.Endpoint)
	u.Path = path.Join(u.Path, s.Bucket, image.ID)
	return u.String() // prints http://foo/bar.html
}
