package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

// Account represents user account
type Account struct {
	ID        string
	Username  string
	Email     string
	Age       int
	CreatedAt time.Time
	UpdatedAt time.Time
}

// Image represents uploaded image metadata
type Image struct {
	ID        string
	AccountID string
	MimeType  string
	CreatedAt time.Time
	UpdatedAt time.Time
}

type ImageRepository interface {
	Insert(Image) (Image, error)
	Get(string) (Image, error)
	Count() (int64, error)
	CountByAccountID(id string) (int64, error)
	ByCreatedAt(int) ([]Image, error)
}

type AccountRepository interface {
	Insert(Account) (Account, error)
	Update(Account) error
	Get(string) (Account, error)
	All() ([]Account, error)
}

type FileStorage interface {
	Store(string, io.Reader) error
	URI(Image) string
}

type App struct {
	Files         FileStorage
	Images        ImageRepository
	Accounts      AccountRepository
	MaxUploadSize int64
}

// CalcRating returns rating for account in percent*100
func (app App) CalcRating(account Account) float64 {
	all, _ := app.Images.Count()
	if all == 0 {
		return 0
	}

	count, _ := app.Images.CountByAccountID(account.ID)

	return 100.0 * float64(count) / float64(all)
}

type apiError struct {
	Message string `json:"message"`
}

func jsonError(w http.ResponseWriter, message string, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(apiError{message})
}

func main() {
	port := "3000"
	bind := ""

	if p := os.Getenv("PORT"); p != "" {
		port = p
	}

	if b := os.Getenv("BIND"); b != "" {
		bind = b
	}

	pg, err := ConnectPostgres(os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal(err)
	}

	var fileStorage FileStorage

	if os.Getenv("FILESTORAGE") == "S3" {
		s3 := &S3FileStorage{
			AccessKey: os.Getenv("S3_ACCESSKEY"),
			SecretKey: os.Getenv("S3_SECRETKEY"),
			Endpoint:  os.Getenv("S3_ENDPOINT"),
			Region:    os.Getenv("S3_REGION"),
			Bucket:    os.Getenv("S3_BUCKET"),
		}

		err = s3.Connect()
		if err != nil {
			log.Fatal(err)
		}

		fileStorage = s3

	} else {
		lfs := LocalFileStorage{
			Dir:       "media",
			URIPrefix: "/media",
		}
		err = lfs.Prepare()

		fileStorage = lfs

		if err != nil {
			log.Fatal(err)
		}
		http.Handle("/media/", http.StripPrefix("/media/", http.FileServer(http.Dir("media"))))
	}

	app := &App{
		Accounts:      &PostgresAccounts{pg},
		Images:        &PostgresImages{pg},
		MaxUploadSize: 10 * 1024 * 1024 * 1024,
		Files:         fileStorage,
	}

	http.HandleFunc("/accounts", accountsListEndpoint(app))
	http.HandleFunc("/account", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "PUT" {
			signupEndpoint(app)(w, r)
			return
		}

		if r.Method == "GET" {
			getAccountEndpoint(app)(w, r)
			return
		}
		jsonError(w, "Use PUT or GET", http.StatusBadRequest)
	})

	http.HandleFunc("/upload", uploadImageEndpoint(app))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		images, _ := app.Images.ByCreatedAt(10)

		for _, x := range images {
			uri := app.Files.URI(x)
			w.Write([]byte(`<h3><a href="` + uri + `">` + uri + `</a></h3>`))
			w.Write([]byte(`<img style="margin-bottom: 20px; max-width: 60%;" src='` + uri + "'>"))
		}
	})

	log.Println("Listen on: " + bind + ":" + port)
	log.Fatal(http.ListenAndServe(bind+":"+port, nil))
}
